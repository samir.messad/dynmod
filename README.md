# Dynmod

## Name
A spreadsheet interface for demographic projections of tropical livestock populations

## Description
DYNMOD, which is a simple herd growth model interface for ruminant livestock populations. By simple, we mean that it can help non-specialists to implement fast and crude ex ante or ex post demographic diagnostics, in various applications as for example livestock population management, herd production estimation or exploration of scenarios in development projects.
Based on simplified demographic equations, DYNMOD simulates the dynamics of the population size and the number of animals produced per year. DYNMOD also calculates live weights, meat production and secondary productions (milk, skin and hides, manure) at population level, as well financial outputs that can be used in more integrated financial calculations (e.g. benefit-costs ratios or internal return rates). Finally, DYNMOD provides crude estimates of the population feeding requirements in dry mater.

## 3 spreadsheet modules

DYNMOD consists in three spreadsheet modules :

- [ ] STEADY1: simulation of the 1-year population production assuming a demographic steady state – with constant sex-by-age structure and growth rate;
- [ ] STEADY2: simulation of the 1-year population production assuming a demographic steady state – with constant adult population size and null growth rate;
- [ ] PROJ: simulation of the population dynamics and production over a period of time in a possibly variable environment (the period can last from 1 to 20 years).

***

## Installation
Simply download the Microsoft Excel files to get started. Note that the workbook for the PROJ demographic projection has an xlsm file extension because it contains a macro that allows you to duplicate the input parameters for up to 20 simulation years. If you have a version of Microsoft Excel that does not support macros (e.g. microsoft Excel for Mac), you can open this file as an ordinary Microsoft Excel file. You will need to manually enter the parameters for the simulation.

## Usage
See the manual 

## Support
maintainer : Samir Messad (samir.messad@cirad.fr).

## Authors
Matthieu lesnoff (matthieu.lesnoff@cirad.fr)

## Acknowledgment
Veronique Alary (CIRAD, Montpellier), Céline Dutilly (CIRAD, Montpellier), Bruno Gérard (ILRI, Addis Ababa), Mario Herrero (ILRI, Nairobi), Renaud Lancelot (CIRAD, Montpellier), Samir Messad (CIRAD, Montpellier), Robert Ouma (ILRI, Nairobi), PAD Project (ICRISAT-Niamey; DGCD Belgium), David Waweru (ILRI, Nairobi).

## Project status
No development planned
